﻿using System;
using System.Configuration;
using System.Globalization;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;


namespace ForgeDeployer
{
    
    static class BlobHelper
    {
        private static string accountName = ConfigurationSettings.AppSettings["BlobStorageAccountName"];
        private static string accountKey = ConfigurationSettings.AppSettings["BlobStorageAccountKey"];
        private static string accountContainer = ConfigurationSettings.AppSettings["BlobStorageContainer"];

        public static CloudBlockBlob GetBlob(string FileName)
        {
            try
            {

                StorageCredentials creds = new StorageCredentials(accountName, accountKey);
                CloudStorageAccount account = new CloudStorageAccount(creds, useHttps: true);

                CloudBlobClient client = account.CreateCloudBlobClient();

                CloudBlobContainer sampleContainer = client.GetContainerReference(accountContainer);
                sampleContainer.CreateIfNotExists();

                CloudBlockBlob blob = sampleContainer.GetBlockBlobReference(FileName);
                return blob;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSasForBlob(CloudBlockBlob blob,
    int sasMinutesValid)
        {
            SharedAccessBlobPermissions permission = SharedAccessBlobPermissions.Delete | SharedAccessBlobPermissions.Read;

            var sasToken = blob.GetSharedAccessSignature(new SharedAccessBlobPolicy()
            {
                Permissions = permission,
                SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(sasMinutesValid)
            });
            return string.Format(CultureInfo.InvariantCulture, "{0}{1}", blob.Uri.AbsoluteUri, sasToken);
        }
    }
}