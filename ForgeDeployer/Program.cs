﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ForgeDeployer
{
    class Program
    {
        static string sourceDir = ConfigurationSettings.AppSettings["sourceDir"];
        static void Main(string[] args)
        {
            while (true)
            {
                try { 
                //Check if blob finished exist
                CloudBlockBlob blobFinished = BlobHelper.GetBlob("jsonzip/finished.txt");
                if (blobFinished.Exists())
                {

                    // delete published blob
                    CloudBlockBlob blobPublished = BlobHelper.GetBlob("jsonzip/published.txt");
                    blobPublished.DeleteIfExists();

                    // delete error blob
                    CloudBlockBlob blobError = BlobHelper.GetBlob("jsonzip/errorPublish.txt");
                    blobError.DeleteIfExists();

                    // Get zip files

                        string zip1Path = "c:/jsonzip/zip1.zip";
                    CloudBlockBlob blob1 = BlobHelper.GetBlob("jsonzip/zip1.zip");
                    blob1.DownloadToFile(zip1Path, System.IO.FileMode.Create);


                    string zip2Path = "c:/jsonzip/zip2.zip";
                    CloudBlockBlob blob2 = BlobHelper.GetBlob("jsonzip/zip2.zip");
                    blob2.DownloadToFile(zip2Path, System.IO.FileMode.Create);

                    string zip3Path = "c:/jsonzip/zip3.zip";
                    CloudBlockBlob blob3 = BlobHelper.GetBlob("jsonzip/zip3.zip");
                    blob3.DownloadToFile(zip3Path, System.IO.FileMode.Create);

                    string zip4Path = "c:/jsonzip/zip4.zip";
                    CloudBlockBlob blob4 = BlobHelper.GetBlob("jsonzip/zip4.zip");
                    blob4.DownloadToFile(zip4Path, System.IO.FileMode.Create);


                    // Unzip files
                    // copy files to web/public/taxonomy
                    System.IO.Directory.Delete(sourceDir + @"\Web\public\taxonomy", true);
                    System.IO.Directory.CreateDirectory(sourceDir + @"\Web\public\taxonomy");
                    System.IO.Compression.ZipFile.ExtractToDirectory(zip1Path, sourceDir + @"\Web\public\taxonomy");
                    System.IO.Compression.ZipFile.ExtractToDirectory(zip2Path, sourceDir + @"\Web\public\taxonomy");
                    System.IO.Compression.ZipFile.ExtractToDirectory(zip3Path, sourceDir + @"\Web\public\taxonomy");
                    System.IO.Compression.ZipFile.ExtractToDirectory(zip4Path, sourceDir + @"\Web\public\taxonomy");

                    // build react app
                    CallCmd(@"npm run deploy:main:prod", sourceDir + @"\Web");
                    CallCmd(@"npm run deploy:iframe:prod", sourceDir + @"\Web");

                    // publish .net solution to dev
                    CallMsBuild(sourceDir + @"\ForgeWebsite.sln /p:DeployOnBuild=true 
/p:PublishProfile="""+ ConfigurationSettings.AppSettings["publishProfile "] + @""" /p:UserName=" + ConfigurationSettings.AppSettings["publishProfileUser"] + @" / p:Password=" + ConfigurationSettings.AppSettings["publishProfilePassword"]);


                    // delete finished blob
                    blobFinished.DeleteIfExists();

                    // create published blob
                    blobPublished.UploadText(DateTimeOffset.Now.ToString());
                }

                // wait 1 minute
                Thread.Sleep(2000);

                }
                catch (Exception ex)
                {
                    CloudBlockBlob blobError = BlobHelper.GetBlob("jsonzip/errorPublish.txt");
                    blobError.UploadText(DateTimeOffset.Now.ToString() + " Error:" + ex.Message + ex.StackTrace + ((ex.InnerException != null) ? (ex.InnerException.Message + ex.InnerException.StackTrace) : ""));
                    
                }
            }
        }

        private static void CallCmd(string cmd,string dir="")
        {
            if (dir == "")
            {
                dir = sourceDir;
            }

            var cmdRunnerInfo = new ProcessStartInfo
            {
                FileName = "cmd",
                RedirectStandardInput = true,
                WorkingDirectory = dir,
                UseShellExecute=false,
            };
            cmdRunnerInfo.Arguments = "/c \""+cmd+"\"";
            var cmdRunnerProcess = Process.Start(cmdRunnerInfo);
            cmdRunnerProcess.WaitForExit();
        }

        private static void CallMsBuild(string cmd, string dir = "")
        {
            if (dir == "")
            {
                dir = sourceDir;
            }

            var cmdRunnerInfo = new ProcessStartInfo
            {
                FileName = @"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe",
                RedirectStandardInput = true,
                WorkingDirectory = dir,
                UseShellExecute = false,
            };
            cmdRunnerInfo.Arguments = cmd;
            var cmdRunnerProcess = Process.Start(cmdRunnerInfo);
            cmdRunnerProcess.WaitForExit();
        }
    }
}
